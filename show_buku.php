<?php
$Db_nama = "buku";
$Db_user = "root";
$Db_pass = "";
$Db_server = "localhost";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $conn = mysqli_connect($Db_server, $Db_user, $Db_pass, $Db_nama);
    $sql = "SELECT a.id_buku, a.judul_buku ,b.nm_jenis , a.nm_pengarang, a.harga, concat('http://192.168.43.229/buku/images/',photos) as url  FROM bukuinfo a , jenis b  WHERE a.`id_jenis`= b.`id_jenis` ORDER BY b.`nm_jenis`";
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        header("Acces-Control-Allow-Origin: *");
        header("Content-type: application/json; charset=UTF-8");
        $data_buku = array();
        while ($buku = mysqli_fetch_assoc($result)) {
            array_push($data_buku, $buku);
        }
        echo json_encode($data_buku);
    }
}
