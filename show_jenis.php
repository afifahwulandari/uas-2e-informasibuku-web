<?php
$Db_nama = "buku";
$Db_user = "root";
$Db_pass = "";
$Db_server = "localhost";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $conn = mysqli_connect($Db_server, $Db_user, $Db_pass, $Db_nama);
    $sql  = "select id_jenis,nm_jenis from jenis order by nm_jenis asc";
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        header("Acces-Control-Allow-Origin: *");
        header("Content-type: application/json; charset=UTF-8");
        $data_jenis = array();
        while ($jenis = mysqli_fetch_assoc($result)) {
            array_push($data_jenis, $jenis);
        }
        echo json_encode($data_jenis);
    }
}
