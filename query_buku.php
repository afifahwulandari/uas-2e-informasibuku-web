<?php
$Db_nama = "buku";
$Db_user = "root";
$Db_pass = "";
$Db_server = "localhost";
$respon = array();
$respon['kode'] = '000';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $conn = mysqli_connect($Db_server, $Db_user, $Db_pass, $Db_nama);
    $mode = $_POST["mode"];
    switch ($mode) {
        case "insert":
            $id_buku = $_POST['id_buku'];
            $judul_buku = $_POST['judul_buku'];
            $nm_jenis = $_POST['nm_jenis'];
            $nm_pengarang = $_POST['nm_pengarang'];
            $imstr = $_POST['image'];
            $harga = $_POST['harga'];
            $file = $_POST['file'];
            $path       = "images/";

            $sql    = "select id_jenis from jenis where nm_jenis='$nm_jenis'";
            $result = mysqli_query($conn, $sql);
            if (mysqli_num_rows($result) > 0) {
                $data     = mysqli_fetch_assoc($result);
                $id_jenis = $data['id_jenis'];
                $sql      = "insert into bukuinfo(id_buku,judul_buku,id_jenis,nm_pengarang,harga,photos) 
                values('$id_buku','$judul_buku','$id_jenis','$nm_pengarang','$harga','$file')";
                $result   = mysqli_query($conn, $sql);
                if ($result) {
                    if (file_put_contents($path . $file, base64_decode($imstr)) == false) {
                        $sql            = "delete from bukuinfo where id_buku='$id_buku'";
                        mysqli_query($conn, $sql);
                        $respon['kode'] = "111";
                        echo json_encode($respon);
                        exit();
                    } else {
                        echo json_encode($respon);
                        exit();  //insert berhasilll
                    }
                } else {
                    $respon['kode'] = "111";
                    echo json_encode($respon);
                    exit();
                }
            }
            break;

        case "update":
            $id_buku        = $_POST['id_buku'];
            $judul_buku    = $_POST['judul_buku'];
            $nm_jenis       = $_POST['nm_jenis'];
            $nm_pengarang    = $_POST['nm_pengarang'];
            $harga        = $_POST['harga'];
            $imstr      = $_POST['image'];
            $file       = $_POST['file'];
            $path       = "images/";
            $sql = "select id_jenis from jenis where nm_jenis='$nm_jenis'";
            $result = mysqli_query($conn, $sql);
            if (mysqli_num_rows($result) > 0) {
                $data = mysqli_fetch_assoc($result);
                $id_jenis = $data['id_jenis'];

                $sql = "";
                if ($imstr == "") {
                    $sql = "update bukuinfo set judul_buku='$judul_buku', id_jenis='$id_jenis', nm_pengarang='$nm_pengarang', harga='$harga'  where id_buku='$id_buku'";
                    $result = mysqli_query($conn, $sql);

                    if ($result) {
                        echo json_encode($respon);
                        exit();
                    } else {
                        $respon['kode'] = "111";
                        echo json_encode($respon);
                        exit();
                    }
                } else {
                    if (file_put_contents($path . $file, base64_decode($imstr)) == false) {
                        $sql = "delete from bukuinfo where id_buku='$id_buku'";
                        mysqli_query($conn, $sql);
                        $respon['kode'] = "111";
                        echo json_encode($respon);
                        exit();
                    } else {
                        $sql = "update bukuinfo set judul_buku='$judul_buku',id_jenis='$id_jenis', nm_pengarang='$nm_pengarang',
                         harga='$harga',photos='$file' where id_buku='$id_buku'";
                        $result = mysqli_query($conn, $sql);
                        if ($result) {
                            echo json_encode($respon);
                            exit();
                        } else {
                            $respon['kode'] = "111";
                            echo json_encode($respon);
                            exit();
                        }
                    }
                }
            }
            break;

        case "delete":
            $nim = $_POST['id_buku'];
            $sql = "select photos from bukuinfo where id_buku='$nim'";
            $result = mysqli_query($conn, $sql);
            if ($result) {
                if (mysqli_num_rows($result) > 0) {
                    $data = mysqli_fetch_assoc($result);
                    $sampul = $data['photos'];
                    $path = "images/";
                    unlink($path . $sampul);
                }
                $sql = "delete from bukuinfo where id_buku='$nim'";
                $result = mysqli_query($conn, $sql);
                if ($result) {
                    echo json_encode($respon);
                    exit();
                } else {
                    $respon['kode'] = "111";
                    echo json_encode($respon);
                    exit();
                }
            }
            break;
    }
}
